﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EchoBotQuickStart
{
    public static class WordIndex
    {
        private static object _syncRoot = new object();

        private static bool _loaded = false;
        private static string[] _words;
        private static readonly char[] WORD_BREAKS = new char[] { ' ', ',', '\n', '_', ';', '.', ')', '(', '[', ']', '{', '}', '?', '!', '\"', '&', '$', '%', '/', '+', ':', '*', '-', '#', '>', '<', '|' };

        private static string GetFilePath()
        {
            return System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "words.txt");
        }

        private static void Load()
        {
            string[] content = System.IO.File.ReadAllLines(GetFilePath());

            List<string> words = new List<string>();

            foreach (var line in content)
            {
                if (!string.IsNullOrWhiteSpace(line))
                {
                    string word = line.Trim().ToLowerInvariant();
                    words.Add(word);
                    words.Add(word + "s");
                }
            }

            _words = words.OrderBy(w => w).ToArray();
            _loaded = true;
        }

        public static string[] GetWordsFromText(string text, bool unique)
        {
            if (!_loaded)
                Load();

            List<string> words = new List<string>();
            string[] entries = text.Split(WORD_BREAKS);
            foreach (var entry in entries)
            {
                var word = entry.Trim().ToLowerInvariant();
                if (_words.Contains(word) && (!unique || !words.Contains(word)))
                {
                    words.Add(word);
                }
            }
            return words.ToArray();
        }

        public static bool AddWord(string word)
        {
            word = word.Trim().ToLowerInvariant();
            foreach (var c in WORD_BREAKS)
                word = word.Replace(c.ToString(), "");

            if (!string.IsNullOrWhiteSpace(word) && !_words.Contains(word))
            {
                lock (_syncRoot)
                {
                    string path = GetFilePath();
                    List<string> fileWords = System.IO.File.ReadLines(path).ToList();
                    fileWords.Add(word);
                    System.IO.File.WriteAllLines(path, fileWords.ToArray());
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
