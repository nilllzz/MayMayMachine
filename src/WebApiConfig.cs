﻿using Microsoft.Skype.Bots.Events.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Microsoft.Practices.Unity;
using Microsoft.Skype.Bots;
using Microsoft.Skype.Bots.Interfaces;
using Microsoft.Skype.Bots.MessagingService.Model;
using Unity.WebApi;
using System.Threading.Tasks;

namespace EchoBotQuickStart
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API routes
            config.MapHttpAttributeRoutes();

            var container = new UnityContainer();

            RegisterTypes(container);

            config.DependencyResolver = new UnityDependencyResolver(container);
        }

        private static Random rnd = new Random();

        private static void RegisterTypes(UnityContainer container)
        {
            MessagingBotServiceSettings settings = MessagingBotServiceSettings.LoadFromCloudConfiguration();
            IMessagingBotService botService = new BotService(settings);

            container
                .RegisterInstance(botService, new ContainerControlledLifetimeManager());

            botService.OnPersonalChatMessageReceivedAsync += async receivedEvent =>
            {
                await MessageHandler.Process(receivedEvent, false);
            };

            botService.OnContactAddedAsync +=
                async contactAdded =>
                    await contactAdded.Reply("Hello " + contactAdded.FromDisplayName + "!").ConfigureAwait(false);

            botService.OnGroupChatMessageReceivedAsync += async receivedEvent =>
            {
                await MessageHandler.Process(receivedEvent, true);
            };
        }
    }
}
