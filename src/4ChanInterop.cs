﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace EchoBotQuickStart
{
    class _4ChanInterop
    {
        private const int ARCHIVE_PAGES = 986;

        static Random rnd = new Random();
        static Dictionary<int, _4ChanPost[]> _archivePages = new Dictionary<int, _4ChanPost[]>();

        private static _4ChanPost[] LoadArchivePage(int pageId)
        {
            string polURL = "http://4archive.org/board/pol/" + pageId;

            var request = WebRequest.CreateHttp(polURL);
            var response = request.GetResponse();
            var responseStream = response.GetResponseStream();

            StreamReader reader = new StreamReader(responseStream);
            string html = reader.ReadToEnd();

            List<int> blockQuoteIndicies = new List<int>();
            int searchOffset = 0;

            while (searchOffset < html.Length)
            {
                int searchResult = html.IndexOf("<blockquote class=\"postMessage\"", searchOffset);
                if (searchResult > 0)
                {
                    blockQuoteIndicies.Add(searchResult);
                    searchOffset = searchResult + "<blockquote class=\"postMessage\"".Length;
                }
                else
                {
                    searchOffset = html.Length;
                }
            }

            List<string> blockQuotes = new List<string>();

            foreach (var quoteIndex in blockQuoteIndicies)
            {
                var blockQuote = html.Remove(0, quoteIndex);
                blockQuote = blockQuote.Remove(0, blockQuote.IndexOf(">") + 1);

                blockQuote = blockQuote.Remove(blockQuote.IndexOf("</blockquote>"));

                while (blockQuote.Contains("<a href=\""))
                {
                    var index = blockQuote.IndexOf("<a href=\"");
                    var endIndex = blockQuote.IndexOf("</a>", index);
                    blockQuote = blockQuote.Remove(index, endIndex - index + 4);
                }

                blockQuote = blockQuote.Replace("<br>", "\n");
                blockQuote = blockQuote.Replace("<span class=\"quote\">&gt;", ">");
                blockQuote = blockQuote.Replace("</span>", "");
                blockQuote = blockQuote.Replace("<wbr>", "");
                blockQuote = blockQuote.Replace("</wbr>", "");
                blockQuote = WebUtility.HtmlDecode(blockQuote);
                blockQuote = blockQuote.Replace("</s>", "");
                blockQuote = blockQuote.Replace("<span class=\"abbr\">Comment too long.  to view the full text.", "");

                while (blockQuote.StartsWith("\n") || blockQuote.StartsWith(" "))
                {
                    blockQuote = blockQuote.Remove(0, 1);
                }

                blockQuotes.Add(blockQuote);
            }

            System.Diagnostics.Debug.Print("Loaded archive page with ID " + pageId + " with " + blockQuotes.Count + " posts.");
            return blockQuotes.Select(p => new _4ChanPost(p)).ToArray();
        }

        public static _4ChanPost GetRandomMessage()
        {
            int pageId = rnd.Next(0, ARCHIVE_PAGES + 1);
            _4ChanPost[] posts = null;

            if (!_archivePages.TryGetValue(pageId, out posts))
            {
                posts = LoadArchivePage(pageId);
                _archivePages.Add(pageId, posts);
            }

            return posts[rnd.Next(0, posts.Length)];
        }

        public static _4ChanPost GetRandomMessage(string contains)
        {
            int pageId = rnd.Next(0, ARCHIVE_PAGES + 1);
            _4ChanPost[] posts = null;

            if (!_archivePages.TryGetValue(pageId, out posts))
            {
                posts = LoadArchivePage(pageId);
                _archivePages.Add(pageId, posts);
            }

            foreach (var post in posts)
            {
                if (post.FormattedPost.IndexOf(contains, StringComparison.InvariantCultureIgnoreCase) > -1)
                {
                    return post;
                }
            }

            return null;
        }

    }
}
