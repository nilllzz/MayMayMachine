﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.Skype.Bots;
using Microsoft.Skype.Bots.Filters;
using Microsoft.Skype.Bots.Interfaces;

namespace EchoBotQuickStart.Controllers
{
    public class EchoBotController : BotController
    {
        public EchoBotController(
            IMessagingBotService messageProcessor) : base(messageProcessor, "MayMay Machine")
        { }

        [Route("v1/echo")]
        public override Task<HttpResponseMessage> ProcessMessagingEventAsync()
        {
            return base.ProcessMessagingEventAsync();
        }
    }
}