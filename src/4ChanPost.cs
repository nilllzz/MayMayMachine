﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EchoBotQuickStart
{
    public class _4ChanPost
    {
        public string OriginalPost { get; private set; }

        private string _formattedPost = null;
        private string[] _words = null;

        public string FormattedPost
        {
            get
            {
                if (_formattedPost == null)
                    _formattedPost = PostFormatter.PerformFormat(OriginalPost);

                return _formattedPost;
            }
        }

        public string[] Words
        {
            get
            {
                if (_words == null)
                    _words = WordIndex.GetWordsFromText(FormattedPost, false);

                return _words;
            }
        }

        public _4ChanPost(string post)
        {
            OriginalPost = post;
        }

        private const double PERFECT_TEXT_LENGTH = 100; // perfect text length in chars

        public double CreateRank(string[] words)
        {
            int rank = 0;
            int matched = 0;

            foreach (var word in words)
            {
                int count = Words.Count(w => w == word);
                if (count > 0)
                    matched++;
                rank += count;
            }

            double textLengthValue = FormattedPost.Length;
            if (textLengthValue > PERFECT_TEXT_LENGTH)
                textLengthValue = PERFECT_TEXT_LENGTH - (textLengthValue - PERFECT_TEXT_LENGTH);
            textLengthValue /= PERFECT_TEXT_LENGTH;

            return (rank / (double)Words.Length) * textLengthValue;
        }
    }
}