﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EchoBotQuickStart
{
    public static class PostFormatter
    {
        static Random rnd = new Random();
        static string[] Names = new string[]
        {
            "Malc",
            "Nils",
            "Jey",
            "Hippo",
            "Jeremy"
        };

        public static string PerformFormat(string input)
        {
            input = input.Replace("/pol/", "guys");

            string opName = Names[rnd.Next(0, Names.Length)];
            int searchOffset = 0;
            bool noCharInFront = false;
            bool noCharInBack = false;

            while (input.Remove(0, searchOffset).Contains("OP") || input.Remove(0, searchOffset).Contains("op"))
            {
                int searchResult = input.IndexOf("op", searchOffset, StringComparison.InvariantCultureIgnoreCase);

                if (searchResult > 0)
                {
                    char charBack = input[searchResult - 1];
                    noCharInBack = !char.IsLetter(charBack);
                }
                else
                {
                    noCharInBack = true;
                }
                if (searchResult + 2 < input.Length - 1)
                {
                    char charFront = input[searchResult + 3];
                    noCharInFront = !char.IsLetter(charFront);
                }
                else
                {
                    noCharInFront = true;
                }

                if (noCharInBack && noCharInFront)
                {
                    input = input.Remove(searchResult) + opName + input.Remove(0, searchResult + 2);
                    searchOffset += opName.Length;
                }
                else
                {
                    searchOffset += 2;
                }
            }

            return input;
        }
    }
}