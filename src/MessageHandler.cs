﻿using Microsoft.Skype.Bots.Events.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace EchoBotQuickStart
{
    public static class MessageHandler
    {
        private const string LEARN_WORD_COMMAND = "bot, this is a word: ";
        private static Random rnd = new Random();

        public static async Task Process(MessageReceivedEvent e, bool isGroupMessage)
        {
            string message = e.Content;
            if (message.ToLowerInvariant().StartsWith(LEARN_WORD_COMMAND))
            {
                await LearnWord(e);
            }
            else
            {
                int chance = isGroupMessage ? 25 : 100;
                if (rnd.Next(0, chance) < 100)
                {
                    await Reply(e);
                }
            }
        }

        private static async Task Reply(MessageReceivedEvent e)
        {
            string message = e.Content;

            string[] words = WordIndex.GetWordsFromText(message, true);

            List<_4ChanPost> accumulatedPosts = new List<_4ChanPost>();
            for (int i = 0; i < 1000; i++)
            {
                var post = _4ChanInterop.GetRandomMessage();
                if (!accumulatedPosts.Contains(post))
                    accumulatedPosts.Add(post);

                System.Diagnostics.Debug.Print("Accumulated posts: " + accumulatedPosts.Count + "/1000");
            }

            var rankedPosts = accumulatedPosts.ToDictionary(p => p, p => p.CreateRank(words)).OrderByDescending(k => k.Value);
            string postContent = rankedPosts.First().Key.FormattedPost;

            await e.Reply(postContent);
        }

        private static async Task LearnWord(MessageReceivedEvent e)
        {
            string word = e.Content.Remove(0, LEARN_WORD_COMMAND.Length);

            string replyText = "Alright, I learned the word " + word + ". :3";
            if (!WordIndex.AddWord(word))
            {
                replyText = word + "? Tha ain't no word ma nigga.";
            }
            await e.Reply(replyText);
        }
    }
}